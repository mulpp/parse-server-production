// Example express application adding the parse-server module to expose Parse
// compatible API routes.

var express = require('express');
var ParseServer = require('parse-server').ParseServer;
var path = require('path');

var databaseUri = process.env.DATABASE_URI || process.env.MONGODB_URI;

if (!databaseUri) {
  console.log('DATABASE_URI not specified, falling back to localhost.');
}

var api = new ParseServer({
  databaseURI: databaseUri || 'mongodb://nt_user_db:JWBQ8uMUYUmEQjMPB2sb@ds013250.mlab.com:13250/naturmia_qa',
  cloud: process.env.CLOUD_CODE_MAIN || __dirname + '/cloud/main.js', 
  appId: process.env.APP_ID || 'FrzKSjk0D76NmOVRKHiyoJNVQR7Ok0YZUoLE8Mtz',
  masterKey: process.env.MASTER_KEY || 'UBRn1OK6PWcuZDoyZcmOpK7A4QiBqTL4pFD9ukva', //Add your master key here. Keep it secret!
  serverURL: process.env.SERVER_URL || 'https://naturmia-v2.herokuapp.com/parse',  // Don't forget to change to https if needed
  clientKey: '12oNRN0NQEzHPlUzDBJVJfkvlGNPt1yyxHDO3jP4',
  javascriptKey: process.env.JAVASCRIPT_KEY || '',
  restAPIKey: process.env.REST_API_KEY || 'Q42ZNHsAfXgotR4mwgpjctd8XzZTK6EM98yj4NJT',
  dotNetKey: process.env.DOT_NET_KEY || '',
  push: {
    ios: [
      {
        pfx: 'certs/naturmia_aps_production.p12', // the path and filename to the .p12 file you exported earlier. 
        topic: 'it.naturmia.NaturMia', // The bundle identifier associated with your app
        production: true
      },
      {
        pfx: 'certs/naturmia_aps_development.p12', // the path and filename to the .p12 file you exported earlier. 
        topic: 'it.naturmia.NaturMia', // The bundle identifier associated with your app
        production: false
      }
    ]
  }
});
// Client-keys like the javascript key or the .NET key are not necessary with parse-server
// If you wish you require them, you can set them as options in the initialization above:
// javascriptKey, restAPIKey, dotNetKey, clientKey

var app = express();

// Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));

// Serve the Parse API on the /parse URL prefix
var mountPath = process.env.PARSE_MOUNT || '/parse';
app.use(mountPath, api);

// Parse Server plays nicely with the rest of your web routes
app.get('/', function(req, res) {
  res.status(200).send('I dream of being a website.  Please star the parse-server repo on GitHub!');
});

// There will be a test page available on the /test path of your server url
// Remove this before launching your app
// app.get('/test', function(req, res) {
//   res.sendFile(path.join(__dirname, '/public/test.html'));
// });

var port = process.env.PORT || 1337;
var httpServer = require('http').createServer(app);
httpServer.listen(port, function() {
    console.log('NaturMia V2 Parse Server example running on port ' + port + '.');
});

// This will enable the Live Query real-time server
ParseServer.createLiveQueryServer(httpServer);
