
Parse.Cloud.define('hello', function(req, res) {
  res.success('Hi');
});

var Mailgun = require('mailgun-js')({apiKey: 'key-fda242e1de0c7bd1e855e8be724e3496', domain: 'naturmia.com'});

Parse.Cloud.define("sendFeedbackAsEmail", function(request, response) { 
  var body = "Comment from: " + request.params.senderEmail + "\n\n" + request.params.msgBody;
  var data = {
	  from: "NaturMia iOS Support <ios@naturmia.com>",
	  to: "support@naturmia.com",
	  subject: "[iOS] NaturMia Feedback",
	  text: body
  };
  Mailgun.messages().send(data, function (error, body) {
  	if (error) {
      response.error("Uh oh, something went wrong"+httpResponse);
  	} else {

  		var ios_user = {
		  subscribed: true,
		  address: request.params.senderEmail,
		  name: request.params.senderEmail
		};
		var list = Mailgun.lists('ios-support@naturmia.com');

		list.members().create(ios_user, function(err, data) {
  			console.log(data);
		});
		
      	response.success("Email sent!");
  	}
  });
});